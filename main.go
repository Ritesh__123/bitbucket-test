package main

import (
	"fmt"
	"log"
)

func main() {
	fmt.Println("Hello World!")
	fmt.Println("Okay world")
	Go()
}

func Go() {
	log.Println("Hello")
}
